# 3d Reconstructor Documents

- This repo contains all the test results related to the 3D cloth reconstruction. 
- We have created a system that has capability to scan any physical cloth using multiple depth sensors.
- These results are generated using Intel Realsense D435 sensors.
- valid_modified.ply is the model that was created by our system.
